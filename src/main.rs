use std::io::Write;
use rand::seq::SliceRandom;

enum Relationships {
    Helen,
    Achilles,
    Zeus,
    Poseidon,
    Apollo,
}
use Relationships::*;

const FEMALE: [Relationships; 3] = [Zeus, Poseidon, Apollo]; // Zeus, Poseidon, Apollo
const ENBY: [Relationships; 5] = [Helen, Achilles, Zeus, Poseidon, Apollo]; // Helen, Achilles, Zeus, Poseidon, Apollo
const MALE: [Relationships; 3] = [Helen, Achilles, Apollo]; // Helen, Achilles, Apollo

fn main() {
    println!("Welcome to the Greek Dating Simulator!");

    let gender = read(
        "Select a gender",
        vec!["Female", "Enby Icon (aka chaos mode ig)", "Male"],
    );

    let opts: Vec<Relationships> = match gender {
        0 => FEMALE.into(),
        1 => ENBY.into(),
        2 => MALE.into(),
        _ => panic!("Internal handling error"),
    };
    let choice = opts.choose(&mut rand::thread_rng()).unwrap();

    let n = match choice {
        Helen => "You married Helen. You pissed off the entire population of Greece. They all came and murdered you.",
        Achilles => "You had a nice sweet life with Achilles. You died (because everyone always dies when Achilles likes them)",
        Zeus => "Zeus picked you. You had no choice.\nHera got angry. She hunted and tormented you for years.\nYou died",
        Poseidon => "Poseidon picked you. You had no choice.\nWell done, you didn't die of anything too gnarly.\nCan't say you had a very nice life tho. Sorry about that.",
        Apollo => "Apollo took a fancy to you and murdered you out of principle.",
    };

    println!("\n\n{}", n);
    println!("\n\nTHANKS FOR PLAYING")
}

fn read(prompt: &str, options: Vec<&str>) -> u8 {
    println!("{prompt}");
    println!("Your options:");
    for (i, opt) in options.iter().enumerate() {
        println!("\t[{i}]: {opt}");
    }

    loop {
        print!("Pick your option: ");

        let mut buf = String::new();

        use std::io::{stdin, stdout};
        stdout().flush().unwrap();
        stdin().read_line(&mut buf).unwrap();

        buf = buf.trim().into();

        if buf == String::new() {
            println!("Please choose an option");
            continue;
        }

        let val = match buf.parse::<u8>() {
            Ok(t) => t,
            Err(_) => {
                println!("Please enter a number");
                continue;
            }
        };

        if val as usize > options.len() - 1 {
            println!("Invalid option");
            continue;
        }

        break val;
    }
}
